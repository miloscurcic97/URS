import pytest
from moduli.rad_sa_automobilima import ucitaj


def test_ucitaj():
    automobili = ucitaj()
    assert automobili.automob is not None
    assert type(automobili.automob) is list
    assert len(automobili.automob) == 10



def test_ucitati_none():
    with pytest.raises(ValueError):
        ucitaj(None)
def test_ucitati_int():
    with pytest.raises(TypeError):
        ucitaj(1)