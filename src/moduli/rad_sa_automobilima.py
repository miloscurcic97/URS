from paket.Automobil import automobil;

  
   
def ucitaj():

    """
    note: Ova metoda ucitava vrednosti iz tekstualnog fajla i pretvara ih u objekte automobila i dodaje ih u listu
        
    """
    automob = []
    with open("podaci/automobili", "r") as f:
        for l in f.readlines():
            print(l)
            p = l.strip().split("|")
            i = p[0]
            o = p[1]
            d = p[2]
            v = p[3]
            s = p[4]
            gp = p[5]
            mb = p[6]
            bv = p[7]
            bs = p[8]
            tm = p[9]
            automob.append(automobil(i,o,d,v,s,gp,mb,bv,bs,tm))
        
        return automob
            
                
        

def sacuvaj(automob):

    """
    note: Ova metoda trenutno cuva podatke vezane za automobil
    """

    with open("podaci/automobil.txt", "a") as file:
        for a in automob:
            line = "|".join([a.i,a.o,a.d,a.v,a.s,a.gp, a.mb, a.bs, a.tm]) + '\n'
            file.write(line)
            file.close()


def ispisSvih(automob):

    """
    note: Ispis svih automobila
    """

    for a in automob:
        print(a)

def dodavanje(automob):

    """
    :param arg1: godina proizvodnje automobila
    :type arg1: string
    :param arg2: maksimalna brzina automobila
    :type arg2: string
    :param arg3: text automobila
    :type arg3: string
    :param arg4: opis automobila
    :type arg4: string
    :param arg5: duzina automobila
    :type arg5: string
    :param arg6: sirina automobila
    :type arg6: string
    :param arg7: visina automobila
    :type arg7: string
    :param arg8: broj sedista automobila
    :type arg8: string
    :param arg9: tip menjaca automobila
    :type arg9: string
    note:: Ova metoda dodaje nov automobil sa ispisanim vrednostima korisnika
        """
    oznaka = input('identifikacija: ')
    opis = input('opis: ')
    duzina = input('duzina: ')
    sirina = input('sirina: ')
    visina =  input('visina: ')
    godina = input("godina prozivodnje")
    maksimalna = input("maksimalna brzina")
    broj_sedista = input('broj sedista: ')
    tip_menjaca = input('tip menjaca: ')

    auto = automobil( oznaka, opis,duzina, sirina, visina,godina, maksimalna,  broj_sedista, tip_menjaca)

    automob.append(auto)

def izmena(automob):
    '''
    note:: Izmena dzipova na osnovu unesene oznake
    '''


    oznaka = input('Oznaka automobila za izmenu: ')

    for a in automob:
        if oznaka == a.itd:
            odg = "0"
            while odg not in ["1","2","3","4","5","6","7","8"]:
                print ("Unesite opciju za pretragu"
                       "\n 1. duzina"
                       "\n 2. visina"
                       "\n 3. sirina"
                       "\n 4. godina proizvodnje"
                       "\n 5. maksimalan brzina"
                       "\n 6. broj vrata "
                       "\n 7. broj sedista"
                       "\n 8. tip menjaca")
                odg = input("broj opcije :")
                if odg == "1":
                    duzina = input ("nova duzina: ")
                    a.d = duzina
                    return automob
                elif odg == "2":
                    visina = input ("nova visina: ")
                    a.v = visina
                    return automob                    
                elif odg == "3":
                    sirina = input ("nova sirina: ")
                    a.s = sirina
                    return automob
                elif odg == "4":
                    godina = input ("nova godina proizvodnje: ")
                    a.gp = godina
                    return automob
                elif odg == "5":
                    brzina = input ("nova maksimalna brzina: ")
                    a.mb = brzina
                    return automob 
                elif odg == "6":
                    vrata = input ("nova broj vrata: ")
                    a.bv = vrata
                    return automob
                elif odg == "7":
                    sedista = input ("nov broj sedista: ")
                    a.bs = sedista
                    return automob
                elif odg == "8":
                    menjac = input ("tip menjaca: ")
                    a.tm = menjac
                    return automob

        else:
            print("ne postoji") 
            
def pretragaOznaka(automob):
    '''
    :param arg1: Oznaka za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene oznake
    :param automob:
    '''

    oznaka = input('Unesite oznaku: ')

    for a in automob:

        if oznaka == a.itd:
            print(a)

        else:
            print("ne postoji") 
            
def pretragaDuzina(automob):
    '''
    :param arg1: Duzina za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene duzine
    :param automob:
    '''

    duzina = input('Unesite duzinu: ')

    for a in automob:

        if duzina in automob:
            print(a)
        else:
            print("ne postoji") 
            
def pretragaSirina(automob):
    '''
    :param arg1: Sirina za pretragu automobila
    :type arg1: string
    note: Ova metoda pretrazuje automobil na osnovu unesene sirine
    :param automob:
    '''



    sirina = input('Unesite sirina: ')

    for a in automob:

        if sirina in a.s:
            print(a)
        else:
            print("ne postoji") 
def pretragaVisina(automob):
    '''
    :param arg1: Visina za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene visine
    :param automob:
    '''



    visina = input('Unesite visina: ')

    for a in automob:

        if visina in a.v:
            print(a)
        else:
            print("ne postoji") 
                        
def pretragaBrzina(automob):
    '''
    :param arg1: Maksinalna brzina za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene maksinalne brzine
    :param automob:
    '''

    brzina = input('Unesite maksimalnu brzinu za pretragu: ')

    for a in automob:

        if brzina in a.mb:
            print(a)               
        else:
            print("ne postoji") 
            
def pretragaGodine(automob):
    '''
    :param arg1: Godina proizvodnje za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene godine proizvodnje
    :param automob:
    '''



    brzina = input('Unesite godinu proizvodnje: ')

    for a in automob:

        if brzina in a.gp:
            print(a)                     
        else:
            print("ne postoji")                 
def pretragaVrata(automob):
    '''
    :param arg1: Broj vrata za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesenog broja vrata
    :param automob:
    '''


    vrata = input('Unesite broj vrata za pretragu: ')

    for a in automob:

        if vrata in a.bv:
            print(a)    
        else:
            print("ne postoji")             

def pretragaSedista(automob):
    '''
    :param arg1: Broj sedista za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesenog broja sedista
    :param automob:
    '''

    sedista = input('Unesite broj sedista za pretragu: ')

    for a in automob:

        if sedista in a.bs:
            print(a)    
        else:
            print("ne postoji")     
def pretragaMenjaca(automob):
    '''
    :param arg1: Tip menjaca za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesenog tipa menjaca
    '''

    menjac = input('Unesite tip menjaca: ')

    for a in automob:

        if menjac in a.tm:
            print(a)
        else:
            print("ne postoji")           
            
def sortiranjeBrzina(automob):
    '''
    Sortira i vraca automobile po brzini od vise ka nizoj
    :param automob:
    '''
    print(sorted(automob, key=lambda automob: automob.mb, reverse=True))                


def sortiranjeSedista(automob):
    '''
    Sortira i vraca automobile po broju sedista od viceg ka nizem broju
    :param automob:
    '''
    print(sorted(automob, key=lambda automob: automob.bs, reverse=True))  
                                          
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                           
                                                                   
                                                  