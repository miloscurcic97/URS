from Dzipovi import Dzipovi
from Kvadovi import Kvadovi
from IzlozbeniProstori import IzlozbeniProstori

dzipovi = Dzipovi()
kvadovi = Kvadovi()
izlozbeni_prostori = IzlozbeniProstori()

def main():
    dzipovi.ucitaj()
    kvadovi.ucitaj()
    handleMenu()

def handleMenu():

    opcija = meni()

    if opcija == 'b':
        opcija = meniDzipovi()
        obradiDzip(dzipovi, opcija, izlozbeni_prostori)
    elif opcija == 'c':
        opcija = meniKvadovi()
        obradiKvad(kvadovi, opcija, izlozbeni_prostori)
    elif opcija == 'd':
        print("Izabrali ste izlaz iz programa, dovidjenja!")
        exit()

    handleMenu()

def meni():

    """
    note: Ova metoda pravi ulazni meni
    """

    print("a.Rad sa izlozbenim prostorom")
    print("b.Rad sa dzipom")
    print("c.Rad sa kvadom")
    print("d.Izlaz")

    opcija = raw_input("Unesite opciju:")

    return opcija


def meniKvadovi():
    """
    note: Ova metoda pravi meni za kvadove
    """

    print("a. Prikaz svih kvadova\n")
    print("b. Dodavanje novog kvada\n")
    print("c. Izmena vrednosti kvada\n")
    print("d. Pretraga kvada po\n")
    print("e. Omoguci sortiranje kvada po maksimalnoj brzini\n")
    print("f. Omoguci sortiranje kvada po godini proizvodnje\n")
    print("g. Za kvad prikazati izlozbeni prostor gde se kvad nalazi \n")

    opcija = raw_input("Unesite opciju:")

    return opcija

def meniKvadPretraga():
    """
    note: Ova metoda pravi meni za pretragu kvada
    """

    print("a. Oznaci")
    print("b. Opisu")
    print("d. Duzini")
    print("e. Sirini")
    print("f. Visini")
    print("g. Maksimalnoj brzini")
    print("h. Godini proizvodnje")
    print("i. Pogonu na sva cetiri tocka")
    print("j. Prostor za stvari")

    opcija = raw_input("Unesite opciju: ")

    return opcija

def meniDzipovi():
    """
    note: Ova metoda pravi meni za dzipove
    """

    print("a. Prikaz svih dzipova\n")
    print("b. Dodavanje novog dzipa\n")
    print("c. Izmena vrednosti dzipova\n")
    print("d. Pretraga dzipova po\n")
    print("e. Omoguci sortiranje dzipova po maksimalnoj brzini\n")
    print("f. Omoguci sortiranje dzipova po konjskim snagama\n")
    print("g. Za dzip prikazati izlozbeni prostor gde se dzip nalazi\n")

    opcija = raw_input("Unesite opciju:")

    return opcija



def meniDzipPretraga():
    """
    note: Ova metoda pravi meni za pretragu dzipa
    """

    print("a. Oznaci")
    print("b. Opisu")
    print("c. Duzini")
    print("d. Sirini")
    print("e. Visini")
    print("f. Maksimalnoj brzini")
    print("g. Pogonu na sva cetiri tocka")
    print("h. Godini proizvodnje")
    print("i. Konjskim snagama")
    print("j. Spustajucoj zadnjoj klupi ")


    opcija = raw_input("Unesite opciju: ")

    return opcija

def obradiKvad(kvadovi, opcija, izlozbeni_prostori):
    """
    :param arg1: kvadovi
    :param arg2: opcija
    :param arg3: izlozbeni prostori
    :type arg1: str
    :type arg2: str
    :type arg3: str
    note: Ova metoda pravi meni za obradu kvadova
    """

    if opcija == 'a':
        kvadovi.ispisSvih()
    elif opcija == 'b':
        kvadovi.dodavanje()
    elif opcija == 'c':
        kvadovi.izmena()
    elif opcija == 'e':
        kvadovi.sortiranjePoMaksimalnojBrzini()
    elif opcija == 'f':
        kvadovi.sortiranjePoGodiniProizvodnje()
    else:
        kvadovi.ispisiIzlozbeniProstori(izlozbeni_prostori)

def obradiDzip(dzipovi, opcija, izlozbeni_prostori):
    """
    :param arg1: dzipovi
    :param arg2: opcija
    :param arg3: izlozbeni_prostori
    :type arg1: str
    :type arg2: str
    :type arg3: str
    note: Ova metoda pravi meni za obradu dzipova
    """

    if opcija == 'a':
        dzipovi.ispisSvih()
    elif opcija == 'b':
        dzipovi.dodavanje()
    elif opcija == 'c':
        dzipovi.izmena()
    elif opcija == 'e':
        dzipovi.sortiranjePoMaksimalnojBrzini()
    elif opcija == 'f':
        dzipovi.sortiranjePoKonjskimSnagama()
    else:
        dzipovi.ispisiIzlozbeniProstori(izlozbeni_prostori)

def obradiKvadPretraga(kvadovi, opcija):
    """
    :param arg1: kvadovi
    :param arg2: opcija
    :type arg1: str
    :type arg2: str
    note: Ova metoda u zavisnosti od unete opcije pretrazuje kvad po odrednjenoj vrednosti
        """

    if opcija == 'a':
        kvadovi.pretragaOznaka()

    if opcija == 'b':
        kvadovi.pretragaOpis()

    if opcija == 'c':
        kvadovi.pretragaDuzina()

    if opcija == 'd':
        kvadovi.pretragaSirina()

    if opcija == 'e':
        kvadovi.pretragaVisina()

    if opcija == 'f':
        kvadovi.pretragaMaksimalnaBrzina()

    if opcija == 'g':
        kvadovi.pretragaGodinaProizvodnje()

    if opcija == 'h':
        kvadovi.pretragaPogonNaSvaCetiriTocka()

    if opcija == 'i':
        kvadovi.pretragaProstorZaStvari()



def obradiDzipPretraga(dzipovi, opcija):
    """
    :param arg1: dzipovi
    :param arg2: opcija
    :type arg1: str
    :type arg2: str
    note: Ova metoda u zavisnosti od unete opcije pretrazuje dzip po odrednjenoj vrednosti
    """

    if opcija == 'a':
        dzipovi.pretragaOznaka()

    if opcija == 'b':
        dzipovi.pretragaOpis()

    if opcija == 'c':
        dzipovi.pretragaDuzina()

    if opcija == 'd':
        dzipovi.pretragaSirina()

    if opcija == 'e':
        dzipovi.pretragaVisina()

    if opcija == 'f':
        dzipovi.pretragaMaksimalnaBrzina()

    if opcija == 'g':
        dzipovi.pretragaPogonNaSvaCetiriTocka()

    if opcija == 'h':
        dzipovi.pretragaGodinaProizvodnje()

    if opcija == 'i':
        dzipovi.pretragaKonjskihSnaga()

    if opcija == 'j':
        dzipovi.pretragaSpustajucaZanjaKlupa()


if __name__ == "__main__":
    main()

