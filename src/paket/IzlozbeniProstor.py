
class IzlozbeniProstor(object):

    def __init__(self, oznaka, opis, lokacija):
        '''
            :param arg1: oznaka
            :param arg2: opis
            :param arg3: proizvodjac
            :type arg1: str
            :type arg2: str
            :type arg3: str
            note: Ovo je klasa izlozbenog prostora
        '''

        self.oznaka = oznaka
        self.opis = opis
        self.lokacija = lokacija

        @property
        def oznaka(self):
            return self.oznaka
        @oznaka.settter
        def oznaka(self, value):
            if value <= 0:
                raise("ne sme biti prazno")
            self.__oznaka = value
            
        @property
        def opis(self):
            return self.opis
        @opis.settter
        def opis(self, value):
            if value <= 0:
                raise("ne sme biti prazno")
            self.__opis= value            

        @property
        def lokacija(self):
            return self.lokacija
        @lokacija.settter
        def lokacija(self, value):
            if value <= 0:
                raise("ne sme biti prazno")
            self.__lokacija= value 
            
            
    def __str__(self):

        '''
        note: Ova metoda objekat pretvara u string
        '''

        return "{0} {1} {2}".format(self.oznaka, self.opis, self.lokacija)