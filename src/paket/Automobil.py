'''
Created on May 31, 2018

@author: lord4
'''
from paket.Putnicko import putnicko

        
class automobil(putnicko):
    """
    :param arg1: identifikacija
    :param arg2: duzina
    :param arg3: virina
    :param arg4: sirina
    :param arg5: godina proizvodnje
    :param arg6: maksimalna brzina
    :param arg7: broj vrata
    :param arg8: broj seditsa
    :param arg9: tip menjaca


    note: Ova metoda nasledenju klasu putnicko prosiruje je za broj sedista i tip menjaca
    """

    def __init__(self,idt,opis,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina,broj_vrata, broj_sedista,tip_menjaca):

        putnicko.__init__(self,idt,opis,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina,broj_vrata, broj_sedista, tip_menjaca)
        self.broj_sedista = broj_sedista
        self.tip_menjaca = tip_menjaca
    
    @property
    def broj_sedista(self):
        return self.broj_sedista
    @broj_sedista.setter
    def broj_sedista(self, value):
        if value <= 0:
            raise("mora biti vece on nula")
        self.__broj_sedista = value
    
    @property
    def tip_menjaca(self):
        return self.tip_menjaca
    @tip_menjaca.setter
    def tip_menjaca(self, value):
        if value <= 0:
            raise("mora biti vece on nula")
        self.__tip_menjaca = value
        

    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}".format(self.idt,self.opis, self.duzina,self.visina, self.sirina,self.godina_proizvodnje, self.maksimalna_brzina,
                                                                self.broj_vrata, self.broj_sedista, self.tip_menjaca)   
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                