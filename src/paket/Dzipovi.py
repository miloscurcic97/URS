from Dzip import Dzip

class Dzipovi(object):


    def __init__(self):

        self.dzipovi = []

    def ucitaj(self):

        """
        note: Ova metoda ucitava vrednosti iz tekstualnog fajla i pridaje vrednosti u ,dzip'
        """

        file = open("dzipovi.txt", "r")

        for line in file.readlines():
            line = line[:-1]

            data = line.split('|')

            dzip = Dzip(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9],data[10])

            self.dzipovi.append(dzip)

        file.close()

    def sacuvaj(self):

        """
        note: Ova metoda trenutno cuva podatke vezane za dzipove
        """

        file = open("dzipovi.txt", "w")

        for dzip in self.dzipovi:
            line = "|".join([dzip.oznaka, dzip.opis, dzip.duzina, dzip.sirina, dzip.visina, dzip.maksimalna_brzina,
                             dzip.pogon_na_sva_cetiri_tocka, dzip.godina_proizvodnje, dzip.konjskih_snaga, dzip.spustajuca_zadnja,
                             dzip.izlozbeni_prostor]) + '\n'
            file.write(line)
        file.close()

    def nadjiOznaka(self, oznaka):

        """
        :param arg1: oznaka dzipa
        :type arg1: string
        :return: Pronadjeni dzip
        :rtype: Dzip
        note: Ova metoda na osnovu zadate oznake vraca odgovarajuci dzip
        """


        for dzip in self.dzipovi:

            if dzip.oznaka == oznaka:
                return dzip

        return None

    def ispisSvih(self):

        """
        note:: Ispis svih dzipova
        """

        for dzip in self.dzipovi:
            print(dzip)

    def dodavanje(self):

        """
        note: Ova metoda dodaje nov dzip sa ispisanim vrednostima korisnika
        """

        oznaka = input('oznaka: ')
        opis = input('opis: ')
        duzina = input('duzina: ')
        sirina = input('sirina: ')
        visina =  input('visina: ')
        maksimalna_brzina = input('maksimalna_brzina: ')
        pogon_na_sva_cetiri_tocka = input('pogon na sva cetiri tocka: ')
        godina_proizvodnje = input('godina proizvodnje: ')
        konjskih_snaga = input('konjskih snaga: ')
        spustajuca_zadnja = input('spustajuca zadnja: ')
        izlozbeni_prostor = input('izlozbeni prostor: ')

        dzip = Dzip(oznaka, opis, duzina, sirina, visina, maksimalna_brzina , pogon_na_sva_cetiri_tocka, godina_proizvodnje,konjskih_snaga, spustajuca_zadnja, izlozbeni_prostor)

        self.dzipovi.append(dzip)
        self.sacuvaj()

    def izmena(self):

        """
        note:: Izmena dzipova na osnovu unesene oznake
        """

        oznaka = input('Oznaka: ')

        dzip = self.nadjiOznaka(oznaka)

        if dzip == None:
            return

        opis = input('Opis: ')
        duzina = input('duzina: ')
        sirina = input('sirina: ')
        visina = input('visina: ')
        maksimalna_brzina = input('maksimalna brzina: ')
        pogon_na_sva_cetiri_tocka = input('pogon na sva cetiri tocka: ')
        godina_proizvodnje = input('godina proizvodnje: ')
        konjskih_snaga = input('konjskih_snaga: ')
        spustajuca_zadnja = input('spustajuca_zadnja: ')
        izlozbeni_prostor = input(' izlozbeni prostor : ')
        dzip.opis = opis
        dzip.duzina = duzina
        dzip.sirina = sirina
        dzip.visina = visina
        dzip.maksimalna_brzina = maksimalna_brzina
        dzip.pogon_na_sva_cetiri_tocka = pogon_na_sva_cetiri_tocka
        dzip.godina_proizvodnje = godina_proizvodnje
        dzip.konjskih_snaga = konjskih_snaga
        dzip.spustajuca_zadnja= spustajuca_zadnja
        dzip.izlozbeni_prostor = izlozbeni_prostor
        self.sacuvaj()

    def pretragaOznaka(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene oznake
        """

        oznaka = input('Unesite oznaku: ')

        for dzip in self.dzipovi:

            if oznaka in dzip.oznaka:
                print(dzip)

    def pretragaOpis(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesenog opisa
        """

        opis = input('Unesite opis: ')

        for dzip in self.dzipovi:

            if opis in dzip.opis:
                print(dzip)

    def pretragaDuzina(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene duzine
        """

        duzina = input('Unesite duzinu: ')

        for dzip in self.dzipovi:

            if duzina in dzip.duzina:
                print(dzip)

    def pretragaSirina(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene sirine
        """

        sirina = input('Unesite sirina: ')

        for dzip in self.dzipovi:

            if sirina in dzip.sirina:
                print(dzip)

    def pretragaVisina(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene visine
        """

        visina = input('Unesite visina: ')

        for dzip in self.dzipovi:

            if visina in dzip.visina:
                print(dzip)

    def pretragaMaksimalnaBrzina(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene brzine
        """

        visina = input('Unesite visina: ')

        for dzip in self.dzipovi:

            if visina in dzip.visina:
                print(dzip)

    def pretragaGodinaProizvodnje(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene godine proizvodnje
        """

        godina_proizvodnje = input('Unesite godinu prozvodnje: ')

        for dzip in self.dzipovi:

            if godina_proizvodnje in dzip.godina_proizvodnje:
                print(dzip)

    def pretragaPogonNaSvaCetiriTocka(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene pogon na 4 tocka
        """

        pogoon_na_sva_cetiri_tocka = input('Unesite pogon: ')

        for dzip in self.dzipovi:

            if pogoon_na_sva_cetiri_tocka in dzip.pogon_na_sva_cetiri_tocka:
                print(dzip)

    def pretragaKonjskihSnaga(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene konjske snage
        """

        konjskih_snaga = input('Unesite konjske_snage: ')

        for dzip in self.dzipovi:

            if konjskih_snaga in dzip.konjskih_snaga:
                print(dzip)

    def pretragaSpustajucaZanjaKlupa(self):

        """
        note: Ova metoda pretrazuje dzip na osnovu unesene spustajuce
        """

        spustajuca_zadnja = input('Unesite spustajucu zadnju: ')

        for dzip in self.dzipovi:

            if spustajuca_zadnja in dzip.spustajuca_zadnja:
                print(dzip)

    def sortiranjePoMaksimalnojBrzini(self):

        """
        note: Ova metoda sortira dzip po maksimalnoj brzini
        """

        lista = sorted(self.dzipovi, key=lambda x: x.maksimalna_brzina, reverse=True)

        for dzip in lista:
            print(dzip)

    def sortiranjePoKonjskimSnagama(self):

        """
        note: Ova metoda sortira dzip po konjskim snagama
        """

        lista = sorted(self.dzipovi, key=lambda x: x.konjskih_snaga, reverse=True)

        for dzip in lista:
            print(dzip)

    def ispisiIzlozbeniProstori(self, izlozbeni_prostori):

        """
        :param: lista prostora
        :type:list
        note:
        """

        oznaka = input('Oznaka: ')

        dzip = self.nadjiOznaka(oznaka)

        if dzip == None:
            return

        for izlozbeni_prostor in izlozbeni_prostori:

            if izlozbeni_prostor.oznaka == dzip.izlozbeni_prostor:
                print(izlozbeni_prostor)