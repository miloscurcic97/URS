from Identifikacija import Identifikacija
from Dimenzije import Dimenzija

class Deo(Identifikacija, Dimenzija):

    def __init__(self, oznaka, opis, duzina, sirina, visina):
        """
                :param arg1: oznaka
                :param arg2: opis
                :param arg3: duzina
                :param arg4: sirina
                :param arg5: visina
                :type arg1: str
                :type arg2: str
                :type arg3: str
                :type arg4: str
                :type arg5: str
                note: Ova metoda pravi novu dimenziju na osnovu unetih parametara
                """

        self.oznaka = oznaka
        self.opis = opis
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina

        def __str__(self):
            """
            note: Ova metoda objekat pretvara u string
            """

            return "{0} {1} {2} {3} {4}".format(self.oznaka, self.opis, self.duzina,
                            self.sirina, self.visina)
        
