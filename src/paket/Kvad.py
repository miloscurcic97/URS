class Kvad():

    def __init__(self, oznaka, opis, duzina, sirina, visina, maksimalna_brzina, godina_proizvodnje,
                 pogon_na_sva_cetiri_tocka, prostor_za_stvari, izlozbeni_prostor):
        """
        :param arg1: oznaka
        :param arg2: opis
        :param arg3: duzina
        :param arg4: sirina
        :param arg5: visina
        :param arg6: maksimalna_brzina
        :param arg7: godina_proizvodnje
        :param arg8: pogon_na_sva_cetiri_tocka
        :param arg9: prostor_za_stvari
        :param arg10: izlozbeni_prostor
        :type arg1: str
        :type arg2: str
        :type arg3: str
        :type arg4: str
        :type arg5: str
        :type arg6: str
        :type arg7: str
        :type arg8: str
        :type arg9: str
        :type arg10: str
        note: Ova metoda pravi liste
        """
        self.oznaka = oznaka
        self.opis = opis
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
        self.maksimalna_brzina = maksimalna_brzina
        self.godina_proizvodnje = godina_proizvodnje
        self.pogon_na_sva_cetiri_tocka = pogon_na_sva_cetiri_tocka
        self.prostor_za_stvari = prostor_za_stvari
        self.izlozbeni_prostor = izlozbeni_prostor

    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10}".format(self.oznaka, self.opis
                                                                , self.duzina, self.sirina, self.visina
                                                                , self.maksimalna_brzina
                                                                , self.godina_proizvodnje, self.pogon_na_sva_cetiri_tocka,
                                                                self.prostor_za_stvari, self.izlozbeni_prostor)