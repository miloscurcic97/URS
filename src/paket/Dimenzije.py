class Dimenzija(object):


    def __init__(self, duzina, sirina, visina):
        """
                :param arg1: duzina
                :param arg2: sirina
                :param arg3: visina
                :type arg1: str
                :type arg2: str
                :type arg3: str
                note: Ova metoda pravi novu dimenziju na osnovu unetih parametara
                """

        if duzina <= 0 or sirina <= 0 or visina <= 0:
            raise Exception('Velicine manje od nule')

        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina


    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{0} {1} {2}".format(self.duzina, self.sirina, self.visina)

