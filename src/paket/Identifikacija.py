class Identifikacija(object):

    def __init__(self, oznaka, opis):
        self.oznaka = oznaka
        self.opis = opis

    def __str__(self):
        return "{0} {1}".format(self.oznaka, self.opis)


