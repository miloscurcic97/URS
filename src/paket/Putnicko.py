'''
Created on May 31, 2018

@author: lord4
'''
from paket.Vozilo import vozilo;

        
class putnicko(vozilo):
    '''
    :param arg1: broj vrata
  
        
     note:: nasedjuje klasu vozila i prosiruje je za broj vrata
    '''


    def __init__(self,idt,opis ,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina, broj_vrata):

        vozilo.__init__(self,idt,opis,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina,broj_vrata)
        self.broj_vrata  = broj_vrata
    @property
    def broj_vrata(self):
        return self.__broj_vrata    
    
    @broj_vrata.setter
    def broj_vrata(self,value):
        if value <= 0:
            raise ValueError("Broj vrata mora biti veci od nula")
        self.__broj_vrata = value
  


    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{0} {1} {2} {3} {4} {5} {6} {7}".format(self.idt,self.opis, self.duzina, self.visina, self.sirina,self.godina_proizvodnje, self.maksimalna_brzina
                                                                , self.broj_vrata)   
        
        