from Kvad import Kvad

class Kvadovi(object):


    def __init__(self):

        """
        note: Ova metoda je konsturktor za pocetne vrednosti
        """

        self.kvadovi = []

    def ucitaj(self):

        """
        note: Ova metoda ucitava vrednosti iz tekstualnog fajla i pridaje vrednosti u ,,kvadovi''
        """

        file = open("kvadovi.txt", "r")
        for line in file.readlines():
            line = line[:-1]
            data = line.split('|')

            kvadovi = Kvad(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9])

            self.kvadovi.append(kvadovi)

        file.close()

    def sacuvaj(self):

        """
        note: Ova metoda trenutno cuva podatke vezane za kvad
        """

        file = open("kvadovi.txt", "w")

        for kvad in self.kvadovi:
            line = "|".join([kvad.oznaka, kvad.opis, kvad.duzina, kvad.sirina, kvad.visina,
                             kvad.maksimalna_brzina, kvad.godina_proizvodnje, kvad.pogon_na_sva_cetiri_tocka,
                             kvad.prostor_za_stvari, kvad.izlozbeni_prostor]) + '\n'
            file.write(line)
        file.close()

    def nadjiOznaka(self, oznaka):

        """
        :param arg1: oznaka kvada
        :type arg1: string
        :return: Pronadjeni kvad
        :rtype: Kvad
        note: Ova metoda na osnovu zadate oznake vraca odgovaraji kvad
        """

        for kvad in self.kvadovi:

            if kvad.oznaka == oznaka:
                return kvad

        return None

    def ispisSvih(self):

        """
        note:: Ispis svih kvad
        """

        for kvad in self.kvadovi:
            print(kvad)

    def dodavanje(self):

        """
        note: Ova metoda dodaje nov kvad sa ispisanim vrednostima korisnika
        """

        oznaka = input('oznaka: ')
        opis = input('opis: ')
        duzina = input('duzina: ')
        sirina = input('sirina: ')
        visina = input('visina: ')
        maksimalna_brzina = input('maksimalna_brzina: ')
        godina_proizvodnje = input('godina_proizvodnje: ')
        pogon_na_sva_cetiri_tocka = input('pogon_na_sva_cetiri_tocka: ')
        prostor_za_stvari = input('prostor_za_stvari: ')
        izlozbeni_prostor = input('izlozbeni_prostor:')

        kvad = Kvad(oznaka, opis, duzina, sirina, visina, maksimalna_brzina, godina_proizvodnje,
                    pogon_na_sva_cetiri_tocka, prostor_za_stvari, izlozbeni_prostor)

        self.kvadovi.append(kvad)
        self.sacuvaj()

    def izmena(self):

        """
        note:: Izmena kvadova na osnovu unesene oznake
        """

        oznaka = input('Oznaka: ')

        kvad = self.nadjiOznaka(oznaka)

        if kvad == None:
            return

        opis = input('Opis: ')
        duzina = input('duzina: ')
        sirina = input('sirina: ')
        visina = input('visina: ')
        maksimalna_brzina = input('maksimalna_brzina: ')
        godina_proizvodnje = input('godina_proizvodnje: ')
        pogon_na_sva_cetiri_tocka = input('pogon_na_sva_cetiri_tocka: ')
        prostor_za_stvari = input('prostor_za_stvari: ')
        izlozbeni_prostor = input('izlozbeni_prostor:')
        kvad.opis = opis
        kvad.duzina = duzina
        kvad.sirina = sirina
        kvad.visina = visina
        kvad.maksimalna_brzina = maksimalna_brzina
        kvad.godina_proizvodnje = godina_proizvodnje
        kvad.pogon_na_sva_cetiri_tocka = pogon_na_sva_cetiri_tocka
        kvad.prostor_za_stvari = prostor_za_stvari
        kvad.izlozbeni_prostor = izlozbeni_prostor
        self.sacuvaj()

    def pretragaOznaka(self):

        """
        note: Ova metoda pretrazuje kvad na osnovu unesene oznake
        """

        oznaka = input('Unesite oznaku: ')

        for kvad in self.kvadovi:

            if oznaka in kvad.oznaka:
                print(kvad)

    def pretragaOpis(self):

        """
        note: Ova metoda pretrazuje kvad po unesenom opisu
        """

        opis = input('Unesite opis: ')

        for kvad in self.kvadovi:

            if opis in kvad.opis:
                print(kvad)

    def pretragaDuzina(self):

        """
        note: Ova metoda pretrazuje kvad po unesenoj duzini
        """

        duzina = input('Unesite duzinu: ')

        for kvad in self.kvadovi:

            if duzina in kvad.duzina:
                print(kvad)

    def pretragaSirina(self):

        """
        note: Ova metoda pretrazuje kvad po unesenoj sirini
        """

        sirina = input('Unesite sirinu: ')

        for kvad in self.kvadovi:

            if sirina in kvad.sirina:
                print(kvad)

    def pretragaVisina(self):

        """
        note: Ova metoda pretrazuje kvad po unesenoj visini
        """

        visina = input('Unesite visinu: ')

        for kvad in self.kvadovi:

            if visina in kvad.visina:
                print(kvad)

    def pretragaMaksimalnaBrzina(self):

        """
        note: Ova metoda pretrazuje kvad po unesenoj mernoj jedinici
        """

        maksimalna_brzina = input('Unesite maksimalnu brzinu: ')

        for kvad in self.kvadovi:

            if maksimalna_brzina in kvad.maksimalna_brzina:
                print(kvad)

    def pretragaGodinaProizvodnje(self):

        """
        note: Ova metoda pretrazuje kvad po izmerenoj vrednosti
        """

        godina_proizvodnje = input('Unesite godinu proizvodnje: ')

        for kvad in self.kvadovi:
            if godina_proizvodnje in kvad.godina_proizvodnje:
                print(kvad)

    def pretragaPogonNaSvaCetiriTocka(self):

        """
        note: Ova metoda pretrazuje kvad po potrosnji po merenju
        """

        pogon_na_sva_cetiri_tocka = input('Unesite pogon na sva cetiri tocka ')

        for kvad in self.kvadovi:

            if pogon_na_sva_cetiri_tocka in kvad.pogon_na_sva_cetiri_tocka:
                print(kvad)

    def pretragaProstorZaStvari(self):

        """
        note: Ova metoda pretrazuje kvad po broju merenja
        """

        prostor_za_stvari = input('Unesite broj merenja ')

        for kvad in self.kvadovi:

            if prostor_za_stvari in kvad.prostor_za_stvari:
                print(kvad)

    def sortiranjePoGodiniProizvodnje(self):

        """
        note: Ova metoda sortira kvad po godini proizvodnje
        """

        lista = sorted(self.kvadovi, key=lambda x: x.godina_proizvodnje, reverse=True)

        for kvad in lista:
            print(kvad)

    def sortiranjePoMaksimalnojBrzini(self):

        """
        note: Ova metoda sortira kvad po maksimalnoj brzini
        """

        lista = sorted(self.kvadovi, key=lambda x: x.maksimalna_brzina, reverse=True)


    def ispisiIzlozbeniProstori(self, izlozbeni_prostori):

        """
        :param: lista prostora
        :type:list
        note:
        """

        oznaka = input('Oznaka: ')

        kvad = self.nadjiOznaka(oznaka)

        if kvad == None:
            return

        for izlozbeni_prostor in izlozbeni_prostori:

            if izlozbeni_prostor.oznaka == kvad.izlozbeni_prostor:
                print(izlozbeni_prostor)


