from IzlozbeniProstor import IzlozbeniProstor

class IzlozbeniProstori(object):


    def __init__(self):

        self.izlozbeni_prostori = []

    def ucitaj(self):

        """
        note: Ova metoda ucitava vrednosti iz tekstualnog fajla i pridaje vrednosti u ,,izlozbeni_prostor''
        """

        file = open("izlozbeniprostor.txt", "r")

        for line in file.readlines():
            line = line[:-1]

            data = line.split('|')

            izlozbeni_prostor = IzlozbeniProstor(data[0], data[1], data[2])

            self.izlozbeni_prostori.append(izlozbeni_prostor)

        file.close()

    def sacuvaj(self):

        """
        note: Ova metoda trenutno cuva podatke vezane za izlozbeni prostor
        """

        file = open("roboti.txt", "r")

        for izlozbeni_prostor in self.izlozbeni_prostori:
            line = "|".join([izlozbeni_prostor.oznaka, izlozbeni_prostor.opis, izlozbeni_prostor.lokacija]) + '\n'
            file.write(line)
        file.close()

    def nadjiOznaka(self, oznaka):

        """
            :param arg1: oznaka izlozbenog prostora
            :type arg1: string
            :return: Pronadjeni izlozbeni prostor
            :rtype: IzlozbeniProstor
            note: Ova metoda na osnovu zadate oznake vraca odgovarajuci izlozbeni prostor
        """

        for izlozbeni_prostor in self.izlozbeni_prostori:

            if izlozbeni_prostor.oznaka == oznaka:
                return izlozbeni_prostor

        return None

    def ispisSvih(self):

        """
            note:: Ispis svih izlozbenih prostora
        """

        for izlozbeni_prostor in self.izlozbeni_prostori:
            print(izlozbeni_prostor)

    def dodavanje(self):

        """
        note: Ova metoda dodaje novi prostor ispisanim vrednostima korisnika
        """

        oznaka = input('oznaka: ')
        opis = input('opis: ')
        lokacija = input('lokacija: ')

        izlozbeni_prostor = IzlozbeniProstor(oznaka, opis, proizvodjac)

        self.izlozbeni_prostori.append(izlozbeni_prostor)

    def izmena(self):

        """
            note:: Izmena prostora na osnovu unesene oznake
        """

        oznaka = input('Oznaka: ')

        izlozbeni_prostor = self.nadjiOznaka(oznaka)

        if izlozbeni_prostor == None:
            return

        opis = input('Opis: ')
        proizvodjac = input('Proizvodjac: ')
        izlozbeni_prostor.opis = opis
        izlozbeni_prostor.lokacija = lokacija

    def pretragaOznaka(self):

        """
           note: Ova metoda pretrazuje prostor na osnovu unesene oznake
        """

        oznaka = input('Unesite oznaku: ')

        for izlozbeni_prostor in self.izlozbeni_prostori:

            if oznaka in izlozbeni_prostor.oznaka:
                print(izlozbeni_prostor)

    def pretragaOpis(self):

        """
        note: Ova metoda pretrazuje prostor po unesenom opisu
        """

        opis = input('Unesite opis: ')

        for izlozbeni_prostor in self.izlozbeni_prostori:

            if opis in izlozbeni_prostor.opis:
                print(izlozbeni_prostor)

    def pretragaLokacija(self):

        """
        note: Ova metoda pretrazuje prostor po unesenoj lokaciji
        """

        lokacija = input('Unesite lokaciju: ')

        for izlozbeni_prostor in self.izlozbeni_prostori:

            if lokacija in izlozbeni_prostor.lokacija:
                print(izlozbeni_prostor)


    def poveziSaPutnickimVozilom(self, putnickovozilo):

        """
        :param arg1: putnicka vozila prostora
        :type arg1: list
        note: Ova metoda povezuje putnicka vozila sa prostorom
        """

        for izlozbeni_prostor in self.izlozbeni_prostori:

            for deo in putnickovozilo:

                if deo.izlozbeni_prostor == izlozbeni_prostor.oznaka:
                    izlozbeni_prostor.putnickovozilo.append(deo)

    def poveziSaTerenskimVozilom(self, terenskovozilo):

        """
        :param arg1: terensko vozilo prostora
        :type arg1: list
        note: Ova metoda povezuje terensko vozilo sa prostorom
        """

        for izlozbeni_prostor in self.izlozbeni_prostori:

            for deo in terenskovozilo:
                if deo.izlozbeni_prostor == izlozbeni_prostor.oznaka:
                    izlozbeni_prostor.terenskovozilo.append(deo)


    def ispisiVozilo(self):

        """
        note: Ova metoda ispisuje sva vozila prostorapo unesenoj oznaci
        """

        oznaka = input('Unesite oznaku: ')

        izlozbeni_prostor = self.nadjiOznaka(oznaka)

        if izlozbeni_prostor == None:
            return

        for deo in izlozbeni_prostor.terenskovozilo:
            print(deo)

        for deo in izlozbeni_prostor.putnickovozilo:
            print(deo)

