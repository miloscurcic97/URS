'''
Created on Jun 20, 2018

@author: lord4
'''


from moduli import rad_sa_izlozbenim_prostorom
from moduli import rad_sa_automobilima

def meni():
    '''funkcija za prikaz menija i preusmeravanja na funkcije'''
    x = "0"
    print(" 1-Rad sa automobilima."
          "\n 2-Rad sa izlozbenim prostorom."
          "\n 3-Rad sa dzipovima."
          "\n 4-Rad sa kvadovima."
          "\n 5-izlaz iz aplikacije.")
    while x not in ["1","2","3","4","5"]:
        x = input("Unesite broj za zeljenu opciju: ")
    if x == "1":
        print(x)
        x1="0"
        print("izabrali ste opciju Rad sa izlozbenim prostorom"
              "\n 1-Prikaz svih prostora."
              "\n 2-Dodavanje novog automoilma."
              "\n 3-Izmena vrednosti automoilma."
              "\n 4-Pretraga robota po: "
              "\n 5-Sortiranje automobila po brzini."
              "\n 6-Sortiranje automobila po broju sedista.")
        while x1 not in["1","2","3","4","5","6","7"]:
            x1=input("Unesite broj za zeljenu opciju: ")
        if x1 == "1":
            rad_sa_automobilima.ispisSvih()
            meni()
        elif x1 == "2":
            rad_sa_automobilima.dodavanje()
            meni()
        elif x1 == "3":
            rad_sa_automobilima.izmena()
            meni()
        elif x1 == "4":
            print("izaberite ocenu za pretragu "
                  "\n 1.duzini"
                  "\n 2.sirini"
                  "\n 3.visini"
                  "\n 4. maksimalno brzini"
                  "\n 5. godini proizvodenj"
                  "\n 6. broju vrata"
                  "\n 7. broju sedista"
                  "\n 8. tipu menjaca"
                  "\n 9. oznaci") 
            pt = "0"
            while pt not in ["1","2","3","4","5","6","7","8","9"]:
                if pt == "1":
                    rad_sa_automobilima.pretragaDuzina()
                    meni()
                if pt == "2":
                    rad_sa_automobilima.pretragaSirina()
                    meni()
                if pt == "3":
                    rad_sa_automobilima.pretragaVisina()
                    meni()
                if pt == "4":
                    rad_sa_automobilima.pretragaBrzina()
                    meni()
                if pt == "5":
                    rad_sa_automobilima.pretragaGodine()
                    meni()
                if pt == "6":
                    rad_sa_automobilima.pretragaVrata()
                    meni()
                if pt == "7":
                    rad_sa_automobilima.pretragaSedista()
                    meni()
                if pt == "8":
                    rad_sa_automobilima.pretragaMenjaca()
                    meni()
                if pt == "9":
                    rad_sa_automobilima.pretragaOznaka()                    
                    meni()
    elif x=="2":
        izbor = "0"
        
        print("Izaberite opciju:"
        "\n 1.prikazi izlozbene prostore"
        "\n 2.dodaj izlozbene prostore" 
        "\n 3.izmena izlozbenih prostora" 
        "\n 4.pretraga izlozbenih prostora po oznaci" 
        "\n 5.pretraga izlozbenih prostora po lokaciji" 
        "\n 6.pretraga izlozbenih prostora po opisu" 
        "\n 7.prikaz svih vozila izlozbenih protora" 
        "\n 8.prikaz vozila sa najvecom brzinom za odredjeni prostor" 
        "\n 9.prikaz putnickih vozila za odgovarajuci prostor" 
        "\n 10.prikaz terenskih vozila za  prostor")
        izbor = input("Vas izbor (1-11): ")
        
        if izbor == "1":
            rad_sa_izlozbenim_prostorom.prikaz()
            meni()
        elif izbor == '2':
            rad_sa_izlozbenim_prostorom.dodavanje()
            meni()
        elif izbor == '3':
            rad_sa_izlozbenim_prostorom.izmena()
            meni()
        elif izbor == '4':
            rad_sa_izlozbenim_prostorom.pretragaOznaka()
            meni()
        elif izbor == '5':
            rad_sa_izlozbenim_prostorom.pretragaLokacija()
            meni()
        elif izbor == '6':
            rad_sa_izlozbenim_prostorom.pretragaOpis()
            meni()
        elif izbor == '7':
            rad_sa_izlozbenim_prostorom
            meni()
        elif izbor == '()8':
            rad_sa_izlozbenim_prostorom
    elif x == "3":
        print("rad sa _________")  
        
                    
if __name__ == '__main__':
    print("Dobrodosli u aplikaciju!")
    meni()                            
